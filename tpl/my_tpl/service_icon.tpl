<div class="service-fas row hidden-xs">

        <div class="col-sm-4">
            <div class="media-left media-middle">
                <i class="fa fa-truck fa-lg"></i>
            </div>
            <div class="media-body">[{ oxmultilang ident="SW_SERVICE_LIST1" }]</div>
        </div>
        <div class="col-sm-4">
            <div class="media-left media-middle">
                <i class="fa fa-money fa-lg"></i>
            </div>
            <div class="media-body">[{ oxmultilang ident="SW_SERVICE_LIST2" }]</div>
        </div>
        <div class="col-sm-4">
            <div class="media-left media-middle">
                <i class="fa fa-comments-o fa-lg"></i>
            </div>
            <div class="media-body">[{ oxmultilang ident="SW_SERVICE_LIST3" }]</div>
        </div>
    
</div>