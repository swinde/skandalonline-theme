[{block name="dd_widget_sidebar_partners"}]
        <section class="footer-box">
            <div class="h4 footer-box-title">[{oxmultilang ident="PARTNERS"}]</div>
            <div class="footer-box-content">
                [{block name="partner_logos"}]
                [{/block}]
            </div>
        </section>

[{/block}]
