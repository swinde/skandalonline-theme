[{include file="../../flow/tpl/widget/footer/info.tpl"}]
[{block name="sw_footer_information"}]
    <ul class="information list-unstyled">
        <li>
            <a href="[{ oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=psWithdrawalForm" }]" rel="nofollow">[{ oxmultilang ident="WIDGET_SERVICES_WIDERRUFFORMULAR" }]</a>
        </li>
        <li>
            <a href="[{ oxgetseourl ident="OnlineSchlichtung" type="oxcontent" }]" rel="nofollow">OnlineSchlichtung</a>
        </li>
        <li>
            <a href="https://www.janofair.de/janofair-siegel/zertifikat/index.html?shopId=593188&certificateId=a589466e98034ed9ae2ff5a362ea3917"target="_blank"><img src="https://www.janofair.de/export/system/modules/de.janofair.opencms.hibernate/resources/images/shopbetreiber/janofair_siegel_90x90.png" alt="janoFair Logo" border="0"></a>
        </li>
    </ul>
[{/block}]