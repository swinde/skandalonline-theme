<?php
/**
 * This file is part of OXID eShop Community Edition.
 *
 * OXID eShop Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eShop Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop CE
 */

$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(

    'charset'                                   => 'UTF-8',
    'SW_SERVICE_LIST1'                          => 'Schnelle Lieferung<br>Kostenlose Retoure ab 40 €',
    'SW_SERVICE_LIST2'                          => 'Zahlungsmöglichkeiten: Vorkasse, Nachnahme, Paypal, Sofortüberweisung',
    'SW_SERVICE_LIST3'                          => 'Service Hotline +49 (0)3731 217433<br>Mo-Fr von 11:00 -18:00 Uhr',
    'PARTNERS'                                  =>  'Unsere Partner',
    'WIDGET_SERVICES_WIDERRUFFORMULAR'          =>  'Widerrufsformular',
    'DELIVERYTIME_DAY'                                            => 'Werktag',
    'DELIVERYTIME_DAYS'                                           => 'Werktage',
    'EMAIL_ORDER_CUST_HTML_PLUSTAX1'            =>  'MwST.',
    'SHIPPING_VAT2'                             =>  '%',
	// Startseite
    'MANUFACTURERSLIDER_SUBHEAD'                            => 'Wir präsentieren Ihnen hier unsere sorgsam ausgewählten Marken, deren Produkte Sie in unserem Shop finden.',
    'START_BARGAIN_HEADER'                                  => 'Angebote der Woche',
    'START_NEWEST_HEADER'                                   => 'Neu eingetroffen',
    'START_TOP_PRODUCTS_HEADER'                             => 'Unsere beliebtesten Artikel',
    'START_BARGAIN_SUBHEADER'                               => 'Sparen Sie bares Geld mit unseren aktuellen SchnÃ¤ppchen!',
    'START_NEWEST_SUBHEADER'                                => 'Frischer geht es nicht. Gerade noch in der Kiste und nun schon im Shop.',
  'START_TOP_PRODUCTS_SUBHEADER'                          	=> 'Nur 8 Produkte, dafür aber die beliebtesten, die wir Ihnen bieten können.',
);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE" }]
*/